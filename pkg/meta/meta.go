package meta

type Metadata struct {
    Name      string `json:"name,omitempty"`
    Namespace string `json:"namespace,omitempty"`
    Placement string `json:"placement,omitempty"`
}

func NewMetadata() *Metadata{
    return &Metadata{
        Namespace: "default",
    }
}
type Status struct {
    Phase string `json:"phase,omitempty"`
}

func NewStatus() *Status{
    return &Status{
        Phase: "running",
    }
}