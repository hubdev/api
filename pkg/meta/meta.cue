package meta

#Metadata: {
	name:      string
	namespace: string | *"default"
	placement: string
}

#Status: {
	phase: string | *"running"
}
