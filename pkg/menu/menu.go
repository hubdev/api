package menu
import (
    "gitee.com/hubdev/api/pkg/meta"
)
type MenuInfo struct {
    Name     string   `json:"name,omitempty"`
    Catalogs []string `json:"catalogs,omitempty"`
    Icon     string   `json:"icon,omitempty"`
    Url      string   `json:"url,omitempty"`
    Sort     int      `json:"sort,omitempty"`
}

func NewMenuInfo() *MenuInfo{
    return &MenuInfo{
        Icon: "mdi-apps",
    }
}
type Menu struct {
    ApiVersion string         `json:"apiVersion,omitempty"`
    Kind       string         `json:"kind,omitempty"`
    Metadata   *meta.Metadata `json:"metadata,omitempty"`
    Spec       []*MenuInfo    `json:"spec,omitempty"`
}

func NewMenu() *Menu{
    return &Menu{
        ApiVersion: "lb.hubx.dev/v1",
        Kind: "Menu",
    }
}