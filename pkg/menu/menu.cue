package menu

import "gitee.com/hubdev/api/pkg/meta"

#MenuInfo: {
	name?:    string
	catalogs: [...string]
	icon:     string | *"mdi-apps"
	url:      string
	sort:     int
}

#Menu: {
	apiVersion: string | *"lb.hubx.dev/v1"
	kind:       string | *"Menu"
	metadata:   meta.#Metadata
	spec: [...#MenuInfo]
}
