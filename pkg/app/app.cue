package app

import "gitee.com/hubdev/api/pkg/meta"

import "gitee.com/hubdev/api/pkg/input"

#Env: {
	name:  string
	value: string
	from:  string
}

#Port: {
	name:   string
	input:  int
	output: int
}

#Volume: {
	name:   string
	source: string
	target: string
}

#Traits: {
	inputs: [...input.#Route]
}
#Service: {
	name:  string
	type:  string
	image: string
	auto:  bool | *false
	envs: [...#Env]
	ports: [...#Port]
	volumes: [...#Volume]
	traits: #Traits
}
#Spec: {
	services: [...#Service]
}
#App: {
	apiVersion: string | *"core.hubx.dev/v1"
	kind:       string | *"App"
	metadata:   meta.#Metadata
	spec:       #Spec
	status:     meta.#Status
}
