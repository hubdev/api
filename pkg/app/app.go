package app
import (
    "gitee.com/hubdev/api/pkg/meta"
    "gitee.com/hubdev/api/pkg/input"
)
type Env struct {
    Name  string `json:"name,omitempty"`
    Value string `json:"value,omitempty"`
    From  string `json:"from,omitempty"`
}

type Port struct {
    Name   string `json:"name,omitempty"`
    Input  int    `json:"input,omitempty"`
    Output int    `json:"output,omitempty"`
}

type Volume struct {
    Name   string `json:"name,omitempty"`
    Source string `json:"source,omitempty"`
    Target string `json:"target,omitempty"`
}

type Traits struct {
    Inputs []*input.Route `json:"inputs,omitempty"`
}

type Service struct {
    Name    string    `json:"name,omitempty"`
    Type    string    `json:"type,omitempty"`
    Image   string    `json:"image,omitempty"`
    Auto    bool      `json:"auto,omitempty"`
    Envs    []*Env    `json:"envs,omitempty"`
    Ports   []*Port   `json:"ports,omitempty"`
    Volumes []*Volume `json:"volumes,omitempty"`
    Traits  *Traits   `json:"traits,omitempty"`
}
const AutoDefault = false

func NewService() *Service{
    return &Service{
        Auto: AutoDefault,
    }
}
type Spec struct {
    Services []*Service `json:"services,omitempty"`
}

type App struct {
    ApiVersion string         `json:"apiVersion,omitempty"`
    Kind       string         `json:"kind,omitempty"`
    Metadata   *meta.Metadata `json:"metadata,omitempty"`
    Spec       *Spec          `json:"spec,omitempty"`
    Status     *meta.Status   `json:"status,omitempty"`
}
const ApiVersionDefault = "core.hubx.dev/v1"
const KindDefault = "App"

func NewApp() *App{
    return &App{
        ApiVersion: ApiVersionDefault,
        Kind: KindDefault,
    }
}