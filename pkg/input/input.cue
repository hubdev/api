package input
import "gitee.com/hubdev/api/pkg/meta"

#Header: {
	key: string
	val: string
	op:  bool
}

#Api: {
	uri:     string
	backend: string
}

#Traits: {
	hides?: [...string]
	headers?: [...#Header]
	host?:   bool
	cors?:   bool
	xframe?: string
	apis?: [...#Api]
}

#Route: {
	name:     string
	catalogs:  [...string]
	host:     string
	protocol: string
	sort:     int | *0
	port:     int
	uri:      string
	traits?:  #Traits
	target:  string
	backends?: [...string]
}

#Spec: {
	sort: int
	http: [...#Route]
}


#Input: {
	apiVersion: string | *"lb.hubx.dev/v1"
	kind:       string | *"Input"
	metadata:   meta.#Metadata
	spec:       #Spec
}
