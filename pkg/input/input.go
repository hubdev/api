package input
import (
    "gitee.com/hubdev/api/pkg/meta"
)
type Header struct {
    Key string `json:"key,omitempty"`
    Val string `json:"val,omitempty"`
    Op  bool   `json:"op,omitempty"`
}

type Api struct {
    Uri     string `json:"uri,omitempty"`
    Backend string `json:"backend,omitempty"`
}

type Traits struct {
    Hides   []string  `json:"hides,omitempty"`
    Headers []*Header `json:"headers,omitempty"`
    Host    bool      `json:"host,omitempty"`
    Cors    bool      `json:"cors,omitempty"`
    Xframe  string    `json:"xframe,omitempty"`
    Apis    []*Api    `json:"apis,omitempty"`
}

type Route struct {
    Name     string   `json:"name,omitempty"`
    Catalogs []string `json:"catalogs,omitempty"`
    Host     string   `json:"host,omitempty"`
    Protocol string   `json:"protocol,omitempty"`
    Sort     int      `json:"sort,omitempty"`
    Port     int      `json:"port,omitempty"`
    Uri      string   `json:"uri,omitempty"`
    Traits   *Traits  `json:"traits,omitempty"`
    Target   string   `json:"target,omitempty"`
    Backends []string `json:"backends,omitempty"`
}
const SortDefault = 0

func NewRoute() *Route{
    return &Route{
        Sort: SortDefault,
    }
}
type Spec struct {
    Sort int      `json:"sort,omitempty"`
    Http []*Route `json:"http,omitempty"`
}

type Input struct {
    ApiVersion string         `json:"apiVersion,omitempty"`
    Kind       string         `json:"kind,omitempty"`
    Metadata   *meta.Metadata `json:"metadata,omitempty"`
    Spec       *Spec          `json:"spec,omitempty"`
}
const ApiVersionDefault = "lb.hubx.dev/v1"
const KindDefault = "Input"

func NewInput() *Input{
    return &Input{
        ApiVersion: ApiVersionDefault,
        Kind: KindDefault,
    }
}