package des

import (
	"crypto/md5"
	"encoding/hex"
)

func NewLowHighSalt(key string) (string, string) {
	m5 := md5.New()
	m5.Write([]byte("fxl"))
	m5.Write([]byte(key))
	st := m5.Sum(nil)
	length := len(st)
	half := length / 2
	low := st[0 : half-1]
	high := st[half : length-1]
	return hex.EncodeToString(low), hex.EncodeToString(high)
}
