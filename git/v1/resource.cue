package v1

import (
		"gitee.com/hubdev/api/pkg/meta"

)

#Resource: {
	apiVersion: string
	kind:       string
	metadata:   meta.#MetaData
	content:    string
	sha:        string
}
