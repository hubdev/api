package v1
import (
	"gitee.com/hubdev/api/pkg/meta"
)
type Resource struct {
    ApiVersion string         `json:"apiVersion,omitempty"`
    Kind       string         `json:"kind,omitempty"`
    Metadata   *meta.Metadata `json:"metadata,omitempty"`
    Content    string         `json:"content,omitempty"`
    Sha        string         `json:"sha,omitempty"`
}
