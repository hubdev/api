package git

type Author struct {
    Email string `json:"email,omitempty"`
    Name  string `json:"name,omitempty"`
}

type Committer struct {
    Email string `json:"email,omitempty"`
    Name  string `json:"name,omitempty"`
}
