package get

import (
	"fmt"
	"gitee.com/hubdev/api/git/ops"
	v1 "gitee.com/hubdev/api/git/v1"
	"net/http"
	"strings"
)

func init() {
	ops.Register(ops.GetFile, func(url, token string, res *v1.Resource) (*http.Request, error) {
		var api string
		if res.Metadata.Name == "" {
			api = fmt.Sprintf("%s/contents/%ss/%s", url, strings.ToLower(res.Kind), res.Metadata.Namespace)

		} else {
			api = fmt.Sprintf("%s/contents/%ss/%s/%s.yaml", url, strings.ToLower(res.Kind), res.Metadata.Namespace, res.Metadata.Name)

		}
		req, err := http.NewRequest(http.MethodGet, api, nil)
		if err != nil {
			return nil, err
		}
		if token != "" {
			req.Header.Add("Authorization", fmt.Sprintf("token %s", token))
		}
		return req, err
	})
}
