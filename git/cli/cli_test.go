package cli

import (
	"fmt"
	"gitee.com/hubdev/api/git/cli/get"
	"gitee.com/hubdev/api/git/ops"
	v1 "gitee.com/hubdev/api/git/v1"
	"gitee.com/hubdev/api/pkg/meta"

	"testing"
)

func TestCli(t *testing.T) {
	myCli := NewBaseCli("https://xxxx", "appx/repo")
	ctx := myCli.NewCtx(ops.GetFile, &v1.Resource{
		Kind: "App",
		Metadata: &meta.Metadata{
			Namespace: "adx",
			Name:      "secret",
		},
	})
	rsp := &get.Content{}
	ctx.Invoke(rsp)
	fmt.Println(rsp)
}
