package del

type Content struct {
    Name    string `json:"name,omitempty"`
    Path    string `json:"path,omitempty"`
    Sha     string `json:"sha,omitempty"`
    Content string `json:"content,omitempty"`
}
