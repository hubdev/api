package del

import (
	"fmt"
	"gitee.com/hubdev/api/git/ops"
	v1 "gitee.com/hubdev/api/git/v1"
	"net/http"
	"strings"
)

func init() {
	ops.Register(ops.DeleteFile, func(url, token string, res *v1.Resource) (*http.Request, error) {
		rd := strings.NewReader(fmt.Sprintf(`{"sha":"%s"}`, res.Sha))
		api := fmt.Sprintf("%s/contents/%ss/%s/%s.yaml", url, strings.ToLower(res.Kind), res.Metadata.Namespace, res.Metadata.Name)
		req, err := http.NewRequest(http.MethodDelete, api, rd)
		if err != nil {
			return nil, err
		}
		if token != "" {
			req.Header.Add("Authorization", fmt.Sprintf("token %s", token))
		}
		req.Header.Add("Content-Type", "application/json")
		return req, err
	})
}
