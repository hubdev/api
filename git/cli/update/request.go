package update
import (
    "gitee.com/hubdev/api/git"
)
type Request struct {
    Author    git.Author    `json:"author,omitempty"`
    Content   string        `json:"content,omitempty"`
    Branch    string        `json:"branch,omitempty"`
    Committer git.Committer `json:"committer,omitempty"`
    Message   string        `json:"message,omitempty"`
}
