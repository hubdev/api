package update

import (
	"gitee.com/pkg/api/git"
)

#Request: {
	author:    git.#Author
	content:   string
	branch:    string | *"master"
	committer: git.#Committer
	message:   string
}
