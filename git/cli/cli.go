package cli

import (
	"fmt"
	"gitee.com/hubdev/api/git/ops"
	v1 "gitee.com/hubdev/api/git/v1"
)

func New() *Cli {
	return &Cli{
		Url:   fmt.Sprintf("%s/api/v1/repos/%s", "https://lb.hubx.dev:4321", "appx/repo"),
		Token: "",
	}
}
func NewBaseCli(url string, uri string) *Cli {
	return &Cli{
		Url:   fmt.Sprintf("%s/api/v1/repos/%s", url, uri),
		Token: "",
	}
}

type Cli struct {
	Url   string
	Token string
	Body  string
}

func (c *Cli) NewCtx(op ops.Op, res *v1.Resource) *CliContext {
	return &CliContext{
		Url:   c.Url,
		Token: c.Token,
		Op:    op,
		Res:   res,
	}
}
