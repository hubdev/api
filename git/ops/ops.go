package ops

import (
	v1 "gitee.com/hubdev/api/git/v1"
	"net/http"
)

type Op int

const (
	AddFile Op = iota
	UpdateFile
	DeleteFile
	GetFile
)

var (
	OpFuncs = make(map[Op]func(url string, token string, res *v1.Resource) (*http.Request, error), 0)
)

func Register(op Op, fn func(url string, token string, res *v1.Resource) (*http.Request, error)) {
	OpFuncs[op] = fn
}
